/**
 * Created by dev on 26.02.15.
 */

$.widget.bridge('uibutton', $.ui.button);

// Make sidebar menu active
var currentItem = $('.sidebar-menu li a[href="'+window.location.pathname+'"]');
currentItem.parent().addClass("active");
currentItem.parents("li.treeview").addClass("active");

// Asking user about deleting record
$('body').on('click', '.btn-remove', function () {
    return window.confirm("Are you sure you want to delete this record?");
});

// Asking user about deleting selected records
$(".btn-remove-selected").click(function () {

    var checked = $('input[name="id"]:checked').map(function(){
        return $(this).val();
    }).get();

    if (!$.isEmptyObject(checked)) {
        if (window.confirm('Are you sure you want to delete selected records?')) {
            var form = $('<form action="' + this.href + '" method="post">' +
            '<input type="text" name="ids" value="' + checked + '" />' +
            '</form>');
            $('body').append(form);
            form.submit();
        }
    }

    return false;
});

// Toggle all checkboxes by main checkbox
$('body').on('ifToggled', '#selectAllCheckbox', function(){
    var checkboxes = $('input[name="id"]');

    var _self = this;

    $.each(checkboxes, function() {
        if ($(_self).is(':checked')) {
            $(this).iCheck('check');
        } else {
            $(this).iCheck('uncheck');
        }
    });
});

// Set up sorting class to table head
$(".table").find("[data-column='" + window.location.href.urlParams('order') + "']").addClass(
    window.location.href.urlParams('orderDirection') == 'ASC' ? 'sorting_asc' : 'sorting_desc'
);

// Loading table async
function loadTable(url)
{
    $.ajax({
        type: 'GET',
        url: url,
        beforeSend: function() {
            // Fade Table
            $('.box-body').stop(true, true).animate({opacity: 0}, 500);
        },
        success: function(data){
            // Show Table
            $('.box-body').html(data).stop(true, true).animate({opacity: 1}, 0);

            // Change browser url
            window.history.replaceState('', '', url);

            // Make checkboxes pretty
            $('input[type="checkbox"]').iCheck({
                checkboxClass: 'icheckbox_flat-blue',
                radioClass: 'iradio_flat-blue'
            });

            // Set up sorting class to table head
            $(".table").find("[data-column='" + window.location.href.urlParams('order') + "']").addClass(
                window.location.href.urlParams('orderDirection') == 'ASC' ? 'sorting_asc' : 'sorting_desc'
            );
        }
    });
}

// Pagination click
$('body').on('click', 'ul.pagination a', function () {

    if (this.href.indexOf("#") != -1) {
        return false;
    }

    loadTable(this.href);

    return false;
});

// Table header click
$('body').on('click', '[class^="sorting"]', function () {

    var location = window.location.href.urlParams('order', $(this).data('column'))
        .urlParams('orderDirection', $(this).hasClass('sorting_asc') ? 'DESC' : 'ASC');

    loadTable(location);
});

// Search button click
$('#table_search').submit(function(){
    var filter = $(this).find('[name="filter"]').val();

    if (!filter) {
        loadTable(window.location.pathname);
        return false;
    }

    var filterColumn = $(this).find('[name="filterColumn"]').val();

    var location = window.location.pathname.urlParams('filter', filter)
        .urlParams('filterColumn', filterColumn);

    loadTable(location);

    return false;
});
