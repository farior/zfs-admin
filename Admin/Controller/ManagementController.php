<?php
/**
 * Created by PhpStorm.
 * User: dev
 * Date: 02.03.15
 * Time: 15:10
 */

namespace ZFS\Admin\Controller;

use ZFS\Common\Controller\AbstractManagementController;
use Zend\View\Model\ViewModel;

class ManagementController extends AbstractManagementController
{
    /**
     * @permission management
     */
    public function indexAction()
    {
        $this->getEventManager()->trigger('ZFS\Dashboard\Event\Navigation\Sidebar');
        $this->getEventManager()->trigger('ZFS\Dashboard\Event\Dashboard\Total');

        return new ViewModel();
    }
}
