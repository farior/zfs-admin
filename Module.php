<?php
/**
 * Created by PhpStorm.
 * User: dev
 * Date: 24.01.15
 * Time: 12:46
 */

namespace ZFS\Admin;

use Zend\ModuleManager\Feature\ConfigProviderInterface;
use Zend\ModuleManager\Feature\BootstrapListenerInterface;
use Zend\Mvc\MvcEvent;
use Zend\Http\Request;
use Zend\Stdlib\ArrayUtils;
use Zend\EventManager\EventInterface;

class Module implements ConfigProviderInterface, BootstrapListenerInterface
{
    public function onBootstrap(EventInterface $e)
    {
        if (!$e instanceof MvcEvent) {
            return;
        }

        $e->getApplication()
            ->getEventManager()
            ->attach(MvcEvent::EVENT_ROUTE, array($this, 'selectLayoutBasedOnPath'));
    }

    public function selectLayoutBasedOnPath(MvcEvent $e)
    {
        if (!$e->getRequest() instanceof Request) {
            return;
        }

        $layout = $e->getRouteMatch()->getParam('layout', null);

        if ($layout) {
            $e->getViewModel()->setTemplate($layout);
        }
    }

    public function getConfig()
    {
        $config = array();

        $configFiles = array(
            __DIR__ . '/config/module.config.php',
            __DIR__ . '/config/module.config.navigation.php'
        );

        // Merge all module config options
        foreach ($configFiles as $configFile) {
            $config = ArrayUtils::merge($config, include $configFile);
        }

        return $config;
    }
}
