<?php

use ZFCTool\Service\Migration\AbstractMigration;

class Migration_20150302_110645_50 extends AbstractMigration
{
    /**
     * Upgrade
     */
    public function up()
    {
        $this->query("INSERT INTO `zfs_privileges` (`id`, `role_id`, `privilege`) VALUES ('4', '3', 'management')");
    }

    /**
     * Degrade
     */
    public function down()
    {
    }
}
