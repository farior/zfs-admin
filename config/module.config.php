<?php
/**
 * Created by PhpStorm.
 * User: dev
 * Date: 24.01.15
 * Time: 13:10
 */

return array(
    'ZFS\Assets' => array(
        // Assets directory name
        'zfs-admin' => 'assets'
    ),
    'router' => array(
        'routes' => array(
            'management' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route'    => '/management',
                    'defaults' => array(
                        'controller' => 'ZFS\Admin\Controller\Management',
                        'action'     => 'index',
                        'layout'     => 'layout/dashboard'
                    ),
                ),
            ),
        ),
    ),
    'view_manager' => array(
        'template_map' => array(
            'partial/paginator' => __DIR__ . '/../view/partial/paginator.phtml',
            'layout/dashboard' => __DIR__ . '/../view/layout/dashboard.phtml',
            'zfs/management/index' => __DIR__ . '/../view/zfs/management/index.phtml',
        ),
    ),
    'controllers' => array(
        'invokables' => array(
            'ZFS\Admin\Controller\Management' => 'ZFS\Admin\Controller\ManagementController'
        ),
    ),
);
