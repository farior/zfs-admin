<?php
/**
 * Created by PhpStorm.
 * User: dev
 * Date: 02.03.15
 * Time: 15:15
 */
return array(
    'navigation' => array(
        'default' => array(
            array(
                'label'      => 'Management',
                'title'      => 'Management',
                'module'     => 'ZFS\Admin',
                'route'      => 'management',
                'permission' => 'management',
            ),
        )
    )
);
